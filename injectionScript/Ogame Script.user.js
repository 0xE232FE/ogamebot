// ==UserScript==
// @name         Ogame Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://s165-fr.ogame.gameforge.com/*
//@grant GM_xmlhttpRequest
// ==/UserScript==

(function() {
    'use strict';

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = "http://localhost:56428/js/Require.js";
    document.getElementsByTagName('head')[0].appendChild(script);

    console.log("Dependencies for OgameBot added");

    window.addEventListener('load', function() {

        console.log("Loading Ogame bot");

        try {
            require(['http://localhost:56428/js/OgameBot.js']);
        }
        catch(err) {
            console.log("Failed to load script, retrying !");
            require(['http://localhost:56428/js/OgameBot.js']);
        }

    }, false);


    var test = $(".construction:first > tbody > tr").length;
    console.log("Construction: "+test);
})();