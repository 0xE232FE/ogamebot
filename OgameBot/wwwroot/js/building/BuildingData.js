(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./BuildingEnum.js", "../UserData.js"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const BuildingEnum_js_1 = require("./BuildingEnum.js");
    const UserData_js_1 = require("../UserData.js");
    class BuildingData {
        static hasEnoughRessources(building) {
            var build = BuildingEnum_js_1.BuildingEnum.toBuilding(building);
            var receipe = build.getConstructionCost();
            receipe.ressourceMap.forEach((amount, type) => {
                if (UserData_js_1.UserData.object.getRessource(type) < amount) {
                    console.log("Fail to build " + building + " not enough " + type);
                    return false;
                }
            });
            return true;
        }
    }
    exports.BuildingData = BuildingData;
});
