(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./UserData.js", "./BotConfiguration.js", "./building/BuildingController.js"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const UserData_js_1 = require("./UserData.js");
    const BotConfiguration_js_1 = require("./BotConfiguration.js");
    const BuildingController_js_1 = require("./building/BuildingController.js");
    class OgameBot {
        displayTooltip() {
            var toolTip = `
        <div> 
           OmegaBot started <br>
            Version: ${BotConfiguration_js_1.BotConfiguration.version}
        </div>`;
            $("#toolbarcomponent").append(toolTip);
        }
        update() {
            console.log("Updating bot !");
            BuildingController_js_1.BuildingController.object.update();
        }
    }
    exports.OgameBot = OgameBot;
    function startBot() {
        console.log("Ogame bot successfuly started !");
        OgameBot.object = new OgameBot();
        UserData_js_1.UserData.object = new UserData_js_1.UserData();
        BuildingController_js_1.BuildingController.object = new BuildingController_js_1.BuildingController();
        OgameBot.object.displayTooltip();
        setInterval(() => { OgameBot.object.update(); }, BotConfiguration_js_1.BotConfiguration.waitTime);
    }
    setTimeout(() => { startBot(); }, 2000);
});
