﻿import { RessourceEnum } from "./RessourceEnum.js";

export class Receipe {

    ressourceMap: Map<RessourceEnum, number>;

    constructor(ressources: Map<RessourceEnum, number>) {
        this.ressourceMap = ressources;
    }


    static generateReceipeFromWeb(costs: JQuery<JQuery.Node[]>): Receipe {

        var builder = new ReceipeBuilder();

        for (var x = 0; x < costs.children().length; x++) {
            var child : any = costs.children()[x];

            var typeName = child.classList[0] 
            var type = RessourceEnum.stringToRessource(typeName);
            var amount = Number(child.innerText.trim().replace(".", ""));

            builder.setRessource(type, amount);
        }

        return builder.build();

    }
}

class ReceipeBuilder {
    ressourceMap: Map<RessourceEnum, number> = new Map();

    constructor() {
        this.ressourceMap.set(RessourceEnum.metal, 0);
        this.ressourceMap.set(RessourceEnum.crystal, 0);
        this.ressourceMap.set(RessourceEnum.deuterium, 0);
    }

    setRessource(ressource: RessourceEnum, amount: number)
    {
        this.ressourceMap.delete(ressource);
        this.ressourceMap.set(ressource, amount);
    }

    build(): Receipe {
        return new Receipe(this.ressourceMap);
    }
}