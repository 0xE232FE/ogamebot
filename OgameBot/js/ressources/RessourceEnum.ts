﻿export enum RessourceEnum {
    metal = "metal",
    crystal = "crystal",
    deuterium = "deuterium"
}

export namespace RessourceEnum {
    export function stringToRessource(ressourceName: String): RessourceEnum {

        if (ressourceName == "metal")
            return RessourceEnum.metal;
        else if (ressourceName == "crystal")
            return RessourceEnum.crystal;
        else if (ressourceName == "deuterium")
            return RessourceEnum.deuterium;

        throw new Error("Failed to parse ressource name: " + ressourceName);
    }
}