﻿import { RessourceEnum } from "./ressources/RessourceEnum.js";

export class UserData {

    static object : UserData;

    constructor() {
        console.log("Loading user data");
        this.printInfo();
    }


    getRessource(ressource: RessourceEnum): number {

        var tagName : String = null;

        switch (ressource) {
            case RessourceEnum.metal:
                tagName = "#resources_metal";
                break;
            case RessourceEnum.crystal:
                tagName = "#resources_crystal";
                break;
            case RessourceEnum.deuterium:
                tagName = "#resources_deuterium";
                break;
        }

        var a = $(tagName).text();
        return Number(a.replace(".", ""));

    }

    getDarkMatter(): number {
        var a = $("#resources_darkmatter").text();
        return Number(a.replace(".", ""));
    }

    getEnergy(): number {
        var a = $("#resources_energy").text();
        return Number(a.replace(".", ""));
    }

    hasPositiveEnergy(): Boolean{
        if (this.getEnergy() > 0)
            return true;
        return false;
    }


    printInfo() {
        console.log("======== User data information =======");
        console.log(`Metal: ${this.getRessource(RessourceEnum.metal)}`);
        console.log(`Crystals: ${this.getRessource(RessourceEnum.crystal)}`);
        console.log(`Deuterium: ${this.getRessource(RessourceEnum.deuterium)}`);
        console.log(`DarkMatter: ${this.getDarkMatter()}`);
        console.log(`Energy: ${this.getEnergy()}`);
    }
}