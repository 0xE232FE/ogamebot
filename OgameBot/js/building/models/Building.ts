﻿import { Receipe } from "../../ressources/Receipe.js";

export abstract class Building {
    id: number
    name: string

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }

    getConstructionCost() : Receipe {
        var request = $.ajax({
            type: "GET",
            url: "https://s165-fr.ogame.gameforge.com/game/index.php?page=" + this.getBuildType() + "&ajax=1&type=" + this.id,
            async: false
        }).responseText;

        var requestHtml = $.parseHTML(request);
        var find = $(requestHtml).find("#costs");
        return Receipe.generateReceipeFromWeb(find);
  
    }

    expectedUrlToBuild(): string {
        return "?page=" + this.getBuildType();
    }

    abstract getBuildType(): string;
}