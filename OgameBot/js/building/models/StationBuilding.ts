﻿import { Building } from "./Building.js";

export class StationBuilding extends Building {
 
    constructor(id: number, name: string) {
        super(id, name);
    }

    getBuildType(): string {
        return "resources";
    }

}