﻿import { BuildingEnum } from "./BuildingEnum.js";
import { BuildingData } from "./BuildingData.js";
import { UserData } from "../UserData.js";
import { Building } from "./models/Building.js";

export class BuildingController {

    static object: BuildingController;

    update() {
        if (!this.isBuilding()) {

            if (!UserData.object.hasPositiveEnergy()) {
                console.log("Need more power, build solar pannel");
                this.startBuild(BuildingEnum.sunlightCentral);
            }

        }
        else {
            console.log("Is building ... ");
        }

    }



    isBuilding(): Boolean {
        return $(".construction.active:first tr").length > 1 ? true : false;
    }


    private checkGoodUrl(building: Building) {
        var currentLocation = window.location.search;
        if (currentLocation != building.expectedUrlToBuild()) {
            console.log("Changing url to build: " + building.name)
            window.location.search = building.expectedUrlToBuild();
        }
    }

    private getConstructorToken(): string {
        var commandLine = $(".fastBuild:first").attr("onclick");
        var regex= new RegExp('token=([^.]+)\'','g');
        var exe = regex.exec(commandLine);

        return exe[1];
    }

    private sendBuildRequest(build: Building) {
        console.log("Send build request !");

        var url = '?page=' + build.getBuildType() + '&modus=1&type=' + build.id + '&menge=1&token=' + this.getConstructorToken();
        window.location.search = url;
    }

    startBuild(building: BuildingEnum) {

        var build = BuildingEnum.toBuilding(building);

        if (!BuildingData.hasEnoughRessources(building)) {
            console.log("Failed to build cause not egought ressources to build" + building.toString())
        }

        this.checkGoodUrl(build);
        this.sendBuildRequest(build);

    }
}