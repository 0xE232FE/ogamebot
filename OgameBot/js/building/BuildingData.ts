﻿import { BuildingEnum } from "./BuildingEnum.js";
import { UserData } from "../UserData.js";

export class BuildingData {

    static hasEnoughRessources(building: BuildingEnum): Boolean {
        var build = BuildingEnum.toBuilding(building);
        var receipe = build.getConstructionCost();

        receipe.ressourceMap.forEach((amount, type) => {
            if (UserData.object.getRessource(type) < amount) {
                console.log("Fail to build " + building + " not enough " + type);
                return false;
            }
                
        })

        return true;
    }
}